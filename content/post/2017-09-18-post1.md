---
title: Week 1
date: 2017-09-18
tags: ["week1,eerstedag,projectstart"]
---

# 04/09/2017

Gedurende de eerste projectdag hebben we als team een planning gemaakt. In deze planning staat op welke datums we gaan werken aan het project. Ook hebben we gezamelij kbesproken wat we als team willen bereiken gedurende dit project.Verder hebben we meer uitleg gehad over de design challenge van een game maken.

Voor de game moeten wij onderzoek doen van een bepaalde opleiding naar keuze. Het is dan aan het team om gezamelijk een spelconcept te bedenken dat de eerste jaars studenten van die opleiding aan de stad Rotterdam bindt. Ook moeten ze elkaar wat beter leren kennen door middel van de game te spelen.

# 06/09/2017
Gedurende de tweede projectdag is ieder individu begonnen met zijn visuele onderzoek en moodboard.
Deze waren bij iedereen bijna af aan het eind van de dag. Ook op deze dag hebben we meer informatie gekregen over de school en wat we moeten inleveren voor het project. Verder hebben we een workshop gekregen van onze studiecoach over diverse rollen in een team.

# 07/09/2017
Deze dag hebben we als team zelf ingeroosterd. We hebben deze dag gebruikt om onze moodboards en visuele onderzoeken af te ronden.
